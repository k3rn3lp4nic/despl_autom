import wx
import sys
from controlador import usuarioInicio as uI, wrkEscenario as wE, cnxMV as cMV
from threading import Thread
import wx.lib.agw.pyprogress as pp
from json import loads as js
"""
PENDIENTES:
    - Escribir el despliegue de los escenarios en el socketServer
    - Pensar/Crear los otros dos paneles
"""
# Variable globales. Necesarias desde varias clases
# La usan los distintos paneles. Para controlar los directorios en el server
global user
# Se reescribe al usar los distintos paneles. Para acceder al escenario desde
# el hilo que se usa para la barra de progreso (el hilo en realidad ejecuta
# la tarea no la barra de progreso)
global scene

class barraProgreso(pp.PyProgress):
    def __init__(self, mensaje):
        pp.PyProgress.__init__(
                self,
                None,
                title="",
                message=mensaje,
                agwStyle=wx.PD_APP_MODAL
                )

        # Características del widget
        self.SetSize(200,100)
        self.SetGaugeProportion(0.2)
        self.SetGaugeSteps(50)
        self.SetGaugeBackground(wx.WHITE)
        self.SetFirstGradientColour(wx.WHITE)
        self.SetSecondGradientColour(wx.BLUE)

class dialogoRegistro(wx.TextEntryDialog):
    def __init__(self):
        wx.TextEntryDialog.__init__(self,
                None,
                ' ',
                caption='Introduzca el usuario',
                value='Morty')
        
        # Para botones X y CANCEL
        if self.ShowModal() == 5101:
            sys.exit(0)

        global user
        user = self.GetValue()

        chk = uI('chkUsuario', self.GetValue())
        print(chk)
        
        if chk != 'OK':
            incorrecto = wx.MessageDialog(None,
                    """Contacte con el administrador del centro.
                    \n¿Intentar de nuevo?""",
                    'ERROR: El usuario no existe',
                    wx.ICON_ERROR|wx.YES_NO
                    )

            # 5104 = NO || 5103 = YES
            if incorrecto.ShowModal() == 5104:
                sys.exit(0)
            else:
                dialogoRegistro()
    
class panelBotones(wx.Panel):
    def __init__(self, parent, btn):
        wx.Panel.__init__(self, parent)
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        Hsizer = wx.BoxSizer(wx.HORIZONTAL)

        Hsizer.AddStretchSpacer()
        self.btnAtras = wx.Button(self, label='Atrás')
        Hsizer.Add(self.btnAtras, 0, wx.RIGHT)
        Hsizer.Add(5, 0, 0)
        self.btnOk = wx.Button(self, label=btn)
        Hsizer.Add(self.btnOk, 0, wx.RIGHT)
        Hsizer.Add(5, 0, 0)

        sizer.Add(Hsizer, 1, wx.EXPAND)
        sizer.Add(0, 15, 0)

        self.SetSizer(sizer)

class hiloCreacion(Thread):
    def __init__(self, escenario):
        Thread.__init__(self)

        self.scene = escenario

        self.start()

    # Despliegue del escenario. Con un if reutilizamos la clase
    # para el resto de paneles
    def run(self):
        despliegue = wE(
                'creacionEscenario',
                self.scene,
                user
                )

class panelCrear(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.info = wx.StaticText(self,
                label='Seleccione un escenario para más información',
                size=(200,200),
                style=wx.ALIGN_LEFT,
                )

        sizer.AddStretchSpacer()
        sizer.Add(self.info, 3, wx.CENTER)
        sizer.AddStretchSpacer()

        self.escenarios = wx.ComboBox(self,
                size=(200,25),
                choices=['WEB + BBDD', 'SERVIDOR DHCP']
                )

        self.escenarios.Bind(wx.EVT_COMBOBOX, self.cambioInfo)

        sizer.AddStretchSpacer()
        sizer.Add(self.escenarios, 1, wx.CENTER)
        sizer.AddStretchSpacer()

        botones = panelBotones(self, 'Crear')
        sizer.Add(botones, 1, wx.EXPAND)
        
        botones.btnAtras.Bind(wx.EVT_BUTTON, self.Atras)
        botones.btnOk.Bind(wx.EVT_BUTTON, self.OK)

        self.SetSizer(sizer)
        
    def cambioInfo(self, evento):
        opcion = self.escenarios.GetValue()
        print(opcion)
        if opcion == 'WEB + BBDD':
            self.info.SetLabel(
            "El escenario está formado por dos "
            "servidores Apache que dan servicio web. "
            "Un balanceador de carga HAProxy distribuye "
            "las peticiones entre los servidores web. "
            "Además, estos últimos, están conectados "
            "a un servidor de BBDD."
            )
            self.info.Wrap(220)
        elif opcion == 'SERVIDOR DHCP':
            self.info.SetLabel(
            "Escenario simple formado por un servidor "
            "DHCP (dhcpd) y dos clientes"
            )
            self.info.Wrap(220)

    def Atras(self, evento):
        self.Hide()
        self.GetParent().opciones.Show()

    def OK(self, evento):
        """PETICIÓN DE CREACIÓN AL CONTROLADOR"""
        consulta = wE(
                'chkEscenario',
                self.escenarios.GetValue(),
                user
                )

        if consulta == 'NO':
            """SI RESPUESTA NO = NO EXISTE EL ESCENARIO"""
            creacion = hiloCreacion(self.escenarios.GetValue())
            progreso = barraProgreso('Desplegando el escenario...')
            
            # Mientras se esté ejecutando el despliegue animar la barra
            while creacion.is_alive():
                seguir = progreso.UpdatePulse()
            progreso.Destroy()
        else:
            mensaje = wx.MessageDialog(
                    None,
                    'El escenario ya ha sido desplegado. Solo se puede '
                    'desplegar el escenario una vez. Consulte el menú ACCEDER '
                    'A UN ESCENARIO para ver los escenario desplegados.',
                    'No se puede desplegar el escenario',
                    wx.OK|wx.CENTRE
                    )
            mensaje.ShowModal()

class panelAcceder(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        escenarios = js(uI('chkEscenarios', user))

        sizer = wx.BoxSizer(wx.VERTICAL)

        info = wx.StaticText(self,
                label='Seleccione un escenario para ver'
                'las máquinas disponibles.',
                size=(200,150),
                style=wx.ALIGN_LEFT
                )

        sizer.AddStretchSpacer()
        sizer.Add(info, 1, wx.CENTER)
        sizer.Add(0, 15, 0)

        self.eleccionEscenario = wx.ComboBox(self,
                size=(200,25),
                choices=escenarios
                )
        
        self.eleccionEscenario.Bind(wx.EVT_COMBOBOX, self.cambioMV)

        sizer.Add(self.eleccionEscenario, 2, wx.CENTER)
        sizer.AddStretchSpacer()

        self.MVs = wx.ComboBox(self,
                size=(200,25),
                choices=[]
                )

        sizer.Add(self.MVs, 2, wx.CENTER)
        sizer.AddStretchSpacer()

        botones = panelBotones(self, 'Conectar')
        sizer.Add(botones, 1, wx.EXPAND)

        botones.btnAtras.Bind(wx.EVT_BUTTON, self.Atras)
        botones.btnOk.Bind(wx.EVT_BUTTON, self.OK)

        self.SetSizer(sizer)

        # Array con escenarios y MVs disponibles
        self.escenarios = {
                'WEB+BBDD':{
                    'Balanceador':'192.168.33.10',
                    'Servidor Web 1':'192.168.33.12',
                    'Servidor Web 2':'192.168.33.13',
                    'Servidor BBDD':'192.168.33.14'
                    }
                }

    def cambioMV(self, evento):
        escenario = self.eleccionEscenario.GetValue()
        maquinas_escenario = list(self.escenarios[escenario].keys())
        self.MVs.SetItems(maquinas_escenario)

    def Atras(self, evento):
        self.Hide()
        self.GetParent().opciones.Show()

    def OK(self, evento):
        MV = self.MVs.GetValue()
        escenario = self.eleccionEscenario.GetValue()
        direccion = self.escenarios[escenario][MV]
        cMV(direccion)

class hiloEliminacion(Thread):
    def __init__(self, escenarios):
        Thread.__init__(self)

        self.scene = escenarios

        self.start()

    def run(self):
        eliminacion = wE(
                'elimEscenario',
                self.scene,
                user
                )

class panelEliminar(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.VERTICAL)

        info = wx.StaticText(self,
                label='\nSeleccione los escenarios a ser eliminados.',
                size=(200,150),
                style=wx.ALIGN_LEFT
                )
        sizer.Add(info, 1, wx.CENTER)
        sizer.AddStretchSpacer()

        # Reutilizo usuarioInicio porque se envían los mismos datos
        # Obligado a procesar datos en la GUI. PUTA CHAPUZA
        escenarios_desp = js(uI('chkEscenarios', user))
        self.caja = wx.CheckListBox(self,
                size=(200,100),
                choices=escenarios_desp
                )
        sizer.Add(self.caja, 2, wx.CENTER)
        sizer.Add(0, 15, 0)

        botones = panelBotones(self, 'Eliminar')
        sizer.Add(botones, 1, wx.EXPAND)

        botones.btnAtras.Bind(wx.EVT_BUTTON, self.Atras)
        botones.btnOk.Bind(wx.EVT_BUTTON, self.OK)

        self.SetSizer(sizer)

    def Atras(self, evento):
        self.Hide()
        self.GetParent().opciones.Show()

    def OK(self, evento):
        eliminar = self.caja.GetCheckedStrings()
        items = self.caja.GetCheckedItems()
        if len(items) > 0:
            eliminacion = hiloEliminacion(eliminar)
            progreso = barraProgreso('Eliminando los escenarios...')
                
            # Mientras se esté ejecutando el despliegue animar la barra
            while eliminacion.is_alive(): 
                seguir = progreso.UpdatePulse()
            progreso.Destroy()
            mensaje = wx.MessageDialog(None,
                    '\nLos escenarios han sido eliminados',
                    style=wx.OK
                    )
            mensaje.ShowModal()
            for item in items:
                self.caja.Delete(item)
        else:
            mensaje = wx.MessageDialog(None,
                    '\nSeleccione al menos un escenario a eliminar',
                    style=wx.ICON_WARNING
                    )
            mensaje.ShowModal()

class panelOpciones(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        # SIZER
        sizer = wx.BoxSizer(wx.VERTICAL)

        # Botones
        creacion = wx.Button(self, id=1, label='DESPLEGAR UN ESCENARIO')
        self.Bind(wx.EVT_BUTTON, self.GetParent().Crear)
        acceso = wx.Button(self, id=2, label='ACCEDER A UN ESCENARIO')
        acceso.Bind(wx.EVT_BUTTON, self.GetParent().Acceder)
        eliminacion = wx.Button(self, id=3, label='ELIMINAR UN ESCENARIO')
        eliminacion.Bind(wx.EVT_BUTTON, self.GetParent().Eliminar)

        sizer.AddStretchSpacer()
        sizer.Add(creacion, 0, wx.CENTER)
        sizer.Add(0, 15, 0)
        sizer.Add(acceso, 0, wx.CENTER)
        sizer.Add(0, 15, 0)
        sizer.Add(eliminacion, 0, wx.CENTER)
        sizer.AddStretchSpacer()

        self.SetSizer(sizer)
        self.Layout()

class panelCuerpo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.VERTICAL)

        # Paneles
        self.opciones = panelOpciones(self)
        sizer.Add(self.opciones, 1, wx.EXPAND)
        self.creacion = panelCrear(self)
        sizer.Add(self.creacion, 1, wx.EXPAND)
        self.creacion.Hide()
        self.acceso = panelAcceder(self)
        sizer.Add(self.acceso, 1, wx.EXPAND)
        self.acceso.Hide()
        self.eliminacion = panelEliminar(self)
        sizer.Add(self.eliminacion, 1, wx.EXPAND)
        self.eliminacion.Hide()

        self.SetSizer(sizer)

    def Crear(self, evento):
        #self.GetParent().nombre_panel='CREACIÓN'
        self.opciones.Hide()
        self.creacion.Show()
        self.Layout()

    def Eliminar(self, evento):
        self.opciones.Hide()
        self.eliminacion.Show()
        self.Layout()

    def Acceder(self, evento):
        self.opciones.Hide()
        self.acceso.Show()
        self.Layout()

class elFrame(wx.Frame):
    def __init__(self, title, tamaño):
        wx.Frame.__init__(self,
                None,
                title=title,
                size=tamaño)
        
        # Propiedades del frame
        self.SetMaxSize(tamaño)
        self.SetMinSize(tamaño)

        # LOGIN
        registro = dialogoRegistro()

        #####################################################
        #####################################################

        # SIZER
        sizer = wx.BoxSizer(wx.VERTICAL)

        # Contenido
        cuerpo = panelCuerpo(self)
        sizer.Add(cuerpo, 1, wx.EXPAND)

        self.SetSizer(sizer)

        self.Center()
        self.Show()

class Aplicacion(wx.App):
    def OnInit(self):
        frame = elFrame('Aplicación', (325,435))

        return True

    def OnClose(self):
        self.Close()
        
        return True

if __name__ == "__main__":
    app = Aplicacion()
    app.MainLoop()
