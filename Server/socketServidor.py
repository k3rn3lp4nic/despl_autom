import socket
from json import loads as js, dumps as jsd
from os import listdir
from time import sleep
from subprocess import run

# Usuarios
ruta_escenarios = '/home/usu1/Proyectos/GUI_despliegue_automatizado/GUI/Server/'
usuarios = listdir(ruta_escenarios)
print(usuarios)

# Info server

ip = '127.0.0.1'
puerto = 45678

# Creación del socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Vinculación del socket con el server

server = (ip, puerto)
s.bind(server)

def respuesta(mensaje):
    s.sendto(mensaje.encode('utf-8'), direccion)
    print(f'\nRespuesta -> {mensaje}')
    print("==================")

# Server espera peticiones

while True:
    print('Escuchando...')

    # Se reciben los datos
    datos, direccion = s.recvfrom(4096)
    data = js(datos.decode('utf-8'))
    print('=====PETICIÓN=====')
    print(data)
    print(direccion)

    # Se procesan los datos
    usuario = data['usuario']
    # Se envía la respuesta
    if data['accion'] == 'chkUsuario':
        if usuario in usuarios:
            enviar_datos = 'OK'
        else:
            enviar_datos = 'NO OK'
            print(f"Intento de login. Usuario -> {usuario}")

        respuesta(enviar_datos)
    elif data['accion'] == 'chkEscenario':
        escenarios = listdir(ruta_escenarios + usuario)
        escenario = data['escenario'].replace(' ', '')
        
        if escenario in escenarios:
            enviar_datos = 'SI'
            """PASAR A LA OTRA FUNCIÓN EL AVISO INTERNO
            print("Intento de crear escenario existente."
                    f"Escenario -> {escenario}")"""
        else:
            enviar_datos = 'NO'
        
        respuesta(enviar_datos)
    elif data['accion'] == 'creacionEscenario':
        escenario = data['escenario'].replace(' ', '')
        run(f'conf/script_despliegue {usuario} {escenario}',
            shell=True
            )
        if escenario == 'WEB+BBDD':
            print('Llega')
            run('conf/WEB+BBDD/claves_ssh',
                    shell=True
                    )
            run(f'conf/WEB+BBDD/abastecimiento {usuario}',
                    shell=True
                    )
        respuesta('FUNCIONA')
    elif data['accion'] == 'chkEscenarios':
        escenarios = listdir(ruta_escenarios + usuario)
        s.sendto(jsd(escenarios).encode('utf-8'), direccion)
        print(f'\nRespuesta -> {escenarios}')
        print("==================")
    elif data['accion'] == 'elimEscenario':
        escenarios = data['escenario']
        for escenario in escenarios:
            """CREAR SCRIPT DE ELIMINACION"""
            run(f'conf/script_eliminacion {usuario} {escenario}',
                    shell=True
                    )
        respuesta('ELIMINADOS')
